# E-inkish Theme

E-inkish color theme for Code-OSS based apps (VSCode, Codium, code.dev, AzureDataStudio, TheiaIDE and Positron).

Created by sjsepan.

**Enjoy!**

VSCode:
![./images/sjsepan-e-inkish_code.png](./images/sjsepan-e-inkish_code.png?raw=true "VSCode")
Codium:
![./images/sjsepan-e-inkish_codium.png](./images/sjsepan-e-inkish_codium.png?raw=true "Codium")
Code.Dev:
![./images/sjsepan-e-inkish_codedev.png](./images/sjsepan-e-inkish_codedev.png?raw=true "Code.Dev")
Azure Data Studio:
![./images/sjsepan-e-inkish_ads.png](./images/sjsepan-e-inkish_ads.png?raw=true "Azure Data Studio")
TheiaIDE:
![./images/sjsepan-e-inkish_theia.png](./images/sjsepan-e-inkish_theia.png?raw=true "TheiaIDE")
Positron:
![./images/sjsepan-e-inkish_positron.png](./images/sjsepan-e-inkish_positron.png?raw=true "Positron")

## Contact

Steve Sepan

<sjsepan@yahoo.com>

1/9/2025
