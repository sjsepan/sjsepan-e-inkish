# E-inkish Color Theme - Change Log

## [0.1.5]

- tweak widget/notification borders and backgrounds

## [0.1.4]

- update screenshot / readme to indicate add'l supported apps
- canonical layout reorg and fix image links

## [0.1.3]

- update readme and screenshot

## [0.1.2]

- fix source control graph badge colors: brighten scmGraph.historyItemRefColor
- fix titlebar FG/BG in custom mode
- fix folding control FG

## [0.1.1]

- fix manifest and pub WF

## [0.1.0]

- dimmer button BG so as to not compete visually with the project names in the Git sidebar
- fix manifest repo links
- retain v0.0.4 for those who prefer the earlier style
- disabled extensionBadge *and extensionButton* keys, as they were not necessary, and the FG was overriding button FG

## [0.0.4]

- make lst act sel BG transparent

## [0.0.3]

- Fix hidden line highlight border

## [0.0.2]

- Fix FG/BG of sidebar extensions install button when not hovered

## [0.0.1]

- Initial release
