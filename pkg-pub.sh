#scratchpad

# install Node Package Manager

sudo apt install npm

# install vsce

npm install -g @vscode/vsce

# package (see https://gitlab.com/sjsepan/repackage-vsix)

vsce-package-vsix.sh

#login after refreshing token
vsce login username PAT

# publish

vsce publish -i ./sjsepan-e-inkish-0.1.5.vsix
npx ovsx publish sjsepan-e-inkish-0.1.5.vsix --debug --pat <PAT>
